<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Schema;

class Mahasiswa extends Resources {
    protected $table = 'mahasiswa';
    protected $rules = array(
        'name' => 'required|string|max:50',
        'email' => 'required|string|max:50|unique',
        'phone' => 'string|max:12',
        'gender' => 'enum|nullable',
        'address' => 'string|nullable',
        'status' => 'integer|nullable',
        'semester' => 'integer|max:2',
        'total_sks' => 'integer|max:3'
    );
    protected $structures = array(
        "id" => [
            'name' => 'id',
            'label' => 'ID',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => true,
            'type' => 'integer',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],
        "name" => [
            'name' => 'name',
            'label' => 'Name',
            'display' => true,
            'required' => true,
            'validation' => [
                'create' => 'string|max:50',
                'update' => 'string|max:50',
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'string',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Name',
        ],

        "email" => [
            'name' => 'email',
            'label' => 'Email',
            'display' => true,
            'required' => true,
            'validation' => [
                'create' => 'required|string|email|max:255',
                'update' => 'required|string|email|max:255,{id}',
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'string',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Email',
        ],
        "phone" => [
            'name' => 'phone',
            'label' => 'Phone',
            'display' => true,
            'required' => true,
            'validation' => [
                'create' => 'required|string|max:12',
                'update' => 'required|string|max:12,{id}',
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'string',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Phone',
        ],

        "gender" => [
            'name' => 'gender',
            'default' => null,
            'label' => 'Gender',
            'display' => true,
            'required' => true,
            'validation' => [
                'create' => 'required',
                'update' => 'required',
                'delete' => null,
            ],
            'primary' => false,
            'required' => true,
            'type' => 'select',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Gender',
            // Options reference
            // 'reference' => "countries", // Select2 API endpoint => /api/v1/countries
            // 'relationship' => 'country', // relationship request datatable
            'options' => [
                [
                    'value' => 'male',
                    'label' => 'male',
                ],
                [
                    'value' => 'female',
                    'label' => 'female',
                ],
            ],
            'options_disabled' => [],
        ],
        "address" => [
            'name' => 'address',
            'label' => 'address',
            'display' => true,
            'required' => true,
            'validation' => [
                'create' => 'required|string',
                'update' => 'required|string,{id}',
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'textarea',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'address',
        ],
        "status" => [
            'name' => 'status',
            'default' => null,
            'label' => 'status',
            'display' => true,
            'required' => true,
            'validation' => [
                'create' => 'required',
                'update' => 'required',
                'delete' => null,
            ],
            'primary' => false,
            'required' => true,
            'type' => 'select',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'status',
            // Options reference
            // 'reference' => "countries", // Select2 API endpoint => /api/v1/countries
            // 'relationship' => 'country', // relationship request datatable
            'options' => [
                [
                    'value' => 'active',
                    'label' => 'active',
                ],
                [
                    'value' => 'inactive',
                    'label' => 'inactive',
                ],
            ],
            'options_disabled' => [],
        ],
        "semester" => [
            'name' => 'semester',
            'label' => 'semester',
            'display' => true,
            'required' => true,
            'validation' => [
                'create' => 'required|string|max:2',
                'update' => 'required|string|max:2,{id}',
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'integer',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'semester',
        ],
        "total_sks" => [
            'name' => 'total_sks',
            'label' => 'total_sks',
            'display' => true,
            'required' => true,
            'validation' => [
                'create' => 'required|string|max:3',
                'update' => 'required|string|max:3,{id}',
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'integer',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'total_sks',
        ],
        "created_at" => [
            'name' => 'created_at',
            'label' => 'Created At',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'datetime',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],
        "updated_at" => [
            'name' => 'updated_at',
            'label' => 'Updated At',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'datetime',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],
        "deleted_at" => [
            'name' => 'deleted_at',
            'label' => 'Deleted At',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'datetime',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],
        "created_at" => [
            'name' => 'created_at',
            'default' => null,
            'label' => 'Created At',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => false,
            'required' => false,
            'type' => 'datetime',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],
        "updated_at" => [
            'name' => 'updated_at',
            'default' => null,
            'label' => 'Updated At',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => false,
            'required' => false,
            'type' => 'datetime',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],
        "deleted_at" => [
            'name' => 'deleted_at',
            'default' => null,
            'label' => 'Deleted At',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => false,
            'required' => false,
            'type' => 'datetime',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ]
    );
    protected $forms = array(
        [
            [
                'class' => 'col-6',
                'field' => 'name'
            ],
            [
                'class' => 'col-6',
                'field' => 'email'
            ],
            [
                'class' => 'col-6',
                'field' => 'phone'
            ],
            [
                'class' => 'col-6',
                'field' => 'gender'
            ],
            [
                'class' => 'col-6',
                'field' => 'address'
            ],
            [
                'class' => 'col-6',
                'field' => 'status'
            ],
            [
                'class' => 'col-6',
                'field' => 'semester'
            ],
            [
                'class' => 'col-6',
                'field' => 'total_sks'
            ]
        ],
    );


}
