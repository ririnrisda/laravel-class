<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KrsMatkul extends Model
{
    protected static function boot() {
        parent::boot();
        static::observe(Observer::class);
    }
    protected $rules = array(
        'id_krs' => 'required|integer|max:50',
        'id_matakuliah' => 'required|integer|max:50',
    );
    protected $structures = array( 
        "id" => [
            'name' => 'id',
            'label' => 'ID',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => true,
            'type' => 'integer',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],
        "id_krs" => [
            'name' => 'id_krs',
            'label' => 'ID KRS',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => true,
            'type' => 'integer',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],
        "id_matakuliah" => [
            'name' => 'id_matakuliah',
            'label' => 'ID Matakuliah',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => true,
            'type' => 'integer',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],

    );
    use HasFactory;
}
