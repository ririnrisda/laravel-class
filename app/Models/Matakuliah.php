<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matakuliah extends Resources
{
    protected $table = 'mata_kuliah';
    protected $rules = array( 
        'name' => 'required|string|max:50',
        'sks' => 'required|string|max:50',
        'id_jurusan' => 'required|integer',

    );
    protected $structures = array( 
        "id" => [
            'name' => 'id',
            'label' => 'ID',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => true,
            'type' => 'integer',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],
        "name" => [
            'name' => 'name',
            'label' => 'Name',
            'display' => true,
            'required' => true,
            'validation' => [
                'create' => 'string|max:50',
                'update' => 'string|max:50',
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'string',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Name',
        ],
        "sks" => [
            'name' => 'sks',
            'label' => 'SKS',
            'display' => true,
            'required' => true,
            'validation' => [
                'create' => 'string|max:50',
                'update' => 'string|max:50',
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'string',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Name',
        ],
        "id_jurusan" => [
            'name' => 'id_jurusan',
            'default' => null,
            'label' => 'Jurusan',
            'display' => true,
            'validation' => [
                'create' => 'required|integer',
                'update' => 'required|integer',
                'delete' => null,
            ],
            'primary' => false,
            'required' => true,
            'type' => 'reference',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Jurusan',
            // Options reference
            'reference' => "Jurusan", // Select2 API endpoint => /api/v1/countries
            'relationship' => 'jurusan', // relationship request datatable
            'option' => [
                'value' => 'id',
                'label' => 'jurusan'
            ]
        ],
    );
    protected $forms = array( 
        [
            [
                'class' => 'col-6',
                'field' => 'name'
            ],
            [
                'class' => 'col-6',
                'field' => 'sks'
            ],
            [
                'class' => 'col-6',
                'field' => 'id_jurusan'
            ],
        ] 
    );
    public function jurusan() {
        return $this->belongsTo('App\Models\Jurusan', 'id_jurusan', 'id')->withTrashed();
    }

    public function krs() {
        return $this->hasMany('App\Models\Krs', 'id_mata_kuliah', 'id')->withTrashed();
    }

}
