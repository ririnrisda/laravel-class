<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jurusan extends Resources
{
    protected $table = 'jurusan';
    protected $rules = array( 
        'jurusan' => 'required|string|max:50',
    );
    protected $structures = array( 
        "id" => [
            'name' => 'id',
            'label' => 'ID',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => true,
            'type' => 'integer',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],
        "jurusan" => [
            'name' => 'jurusan',
            'label' => 'Jurusan',
            'display' => true,
            'required' => true,
            'validation' => [
                'create' => 'string|max:50',
                'update' => 'string|max:50',
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'string',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Jurusan',
        ],
    );
    protected $forms = array( 
        [
            [
                'class' => 'col-6',
                'field' => 'jurusan'
            ],
        ] 
    );
    public function Matakuliah() {
        return $this->hasMany('App\Models\Matakuliah', 'id_mata_kuliah', 'id');
    }

    public function Krs() {
        return $this->hasMany('App\Models\Krs', 'id_jurusan', 'id');
    }

}
