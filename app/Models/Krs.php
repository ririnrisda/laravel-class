<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Krs extends Resources
{
    protected $table = 'krs';
    protected $rules = array(
        'id_mahasiswa' => 'required|integer|max:50',
        'total_sks' => 'required|integer|max:50',
        'semester' => 'required|integer|max:50',

    );
    protected $structures = array( 
        "id" => [
            'name' => 'id',
            'label' => 'ID',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => true,
            'type' => 'integer',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],
        "id_mahasiswa" => [
            'name' => 'id_mahasiswa',
            'default' => null,
            'label' => 'Mahasiswa',
            'display' => true,
            'validation' => [
                'create' => 'required|integer',
                'update' => 'required|integer',
                'delete' => null,
            ],
            'primary' => false,
            'required' => true,
            'type' => 'reference',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Mahasiswa',
            // Options reference
            'reference' => "Mahasiswa", // Select2 API endpoint => /api/v1/countries
            'relationship' => 'mahasiswa', // relationship request datatable
            'option' => [
                'value' => 'id',
                'label' => 'name'
            ]
        ],
        "total_sks" => [
            'name' => 'total_sks',
            'label' => 'Total sks',
            'display' => true,
            'required' => true,
            'validation' => [
                'create' => 'string|max:50',
                'update' => 'string|max:50',
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'string',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Total SKS',
        ],
        "semester" => [
            'name' => 'semester',
            'label' => 'Semester',
            'display' => true,
            'required' => true,
            'validation' => [
                'create' => 'string|max:50',
                'update' => 'string|max:50',
                'delete' => null,
            ],
            'primary' => false,
            'type' => 'string',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Semester',
        ],
        "id_jurusan" => [
            'name' => 'id_jurusan',
            'default' => null,
            'label' => 'Jurusan',
            'display' => true,
            'validation' => [
                'create' => 'required|integer',
                'update' => 'required|integer',
                'delete' => null,
            ],
            'primary' => false,
            'required' => true,
            'type' => 'reference',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Jurusan',
            // Options reference
            'reference' => "Jurusan", // Select2 API endpoint => /api/v1/countries
            'relationship' => 'jurusan', // relationship request datatable
            'option' => [
                'value' => 'id',
                'label' => 'jurusan'
            ],   
        ],
        "id_mata_kuliah" => [
            'name' => 'id_mata_kuliah',
            'default' => null,
            'label' => 'Mata Kuliah',
            'display' => true,
            'validation' => [
                'create' => 'required|integer',
                'update' => 'required|integer',
                'delete' => null,
            ],
            'primary' => false,
            'required' => true,
            'type' => 'reference',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'MataKuliah',
            // Options reference
            'reference' => "Matakuliah", // Select2 API endpoint => /api/v1/countries
            'relationship' => 'matakuliah', // relationship request datatable
            'option' => [
                'value' => 'id',
                'label' => 'name'
            ],   
        ],
    );
    protected $forms = array(
        [
            [
                'class' => 'col-6',
                'field' => 'id_mahasiswa'
            ],
            [
                'class' => 'col-6',
                'field' => 'semester'
            ],
            [
                'class' => 'col-6',
                'field' => 'id_jurusan'
            ],
            [
                'class' => 'col-6',
                'field' => 'id_mata_kuliah'
            ]
        ]
    );
    public function mahasiswa() {
        return $this->belongsTo('App\Models\Mahasiswa', 'id_mahasiswa', 'id')->withTrashed();
    }
    public function jurusan() {
        return $this->belongsTo('App\Models\Jurusan', 'id_jurusan', 'id')->withTrashed();
    }

  public function Matakuliah() {
        return $this->belongsTo('App\Models\Matakuliah', 'id_mata_kuliah', 'id');
    }

    
    use HasFactory;
}
